#!/bin/bash

# Exit on first error
set -euo pipefail

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)
CONFIG_DIR=${SCRIPT_DIR}/../config
ARTIFACTS_DIR=${SCRIPT_DIR}/../artifacts
CURRENT_DIR=${PWD}

cd ${CONFIG_DIR}

if [ ! -f corda-tools-network-bootstrapper-4.4.jar ] ; then
  wget https://software.r3.com/artifactory/corda-releases/net/corda/corda-tools-network-bootstrapper/4.4/corda-tools-network-bootstrapper-4.4.jar
fi

java -jar corda-tools-network-bootstrapper-4.4.jar

cd ${CURRENT_DIR}

mkdir -p ${ARTIFACTS_DIR}/notary
cp -R ${CONFIG_DIR}/notary/certificates/ ${ARTIFACTS_DIR}/notary/certificates
cp ${CONFIG_DIR}/notary/node.conf ${ARTIFACTS_DIR}/notary/node.conf
cp ${CONFIG_DIR}/notary/network-parameters ${ARTIFACTS_DIR}/shared/network-parameters

mkdir -p ${ARTIFACTS_DIR}/partya
cp -R ${CONFIG_DIR}/partya/certificates/ ${ARTIFACTS_DIR}/partya/certificates
cp ${CONFIG_DIR}/partya/node.conf ${ARTIFACTS_DIR}/partya/node.conf

mkdir -p ${ARTIFACTS_DIR}/partyb
cp -R ${CONFIG_DIR}/partyb/certificates/ ${ARTIFACTS_DIR}/partyb/certificates
cp ${CONFIG_DIR}/partyb/node.conf ${ARTIFACTS_DIR}/partyb/node.conf

mkdir -p ${ARTIFACTS_DIR}/partyc
cp -R ${CONFIG_DIR}/partyc/certificates/ ${ARTIFACTS_DIR}/partyc/certificates
cp ${CONFIG_DIR}/partyc/node.conf ${ARTIFACTS_DIR}/partyc/node.conf

docker-compose -f ${SCRIPT_DIR}/../docker-compose.yaml up -d notary partya partyb partyc

sleep 60
docker-compose -f ${SCRIPT_DIR}/../docker-compose.yaml up -d node-server node-explorer

sleep 10
open http://localhost:3000/ || true
