#!/bin/bash

# Exit on first error
set -euo pipefail

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)
CONFIG_DIR=${SCRIPT_DIR}/../config
ARTIFACTS_DIR=${SCRIPT_DIR}/../artifacts

docker-compose -f ${SCRIPT_DIR}/../docker-compose.yaml down || true

rm -rf ${CONFIG_DIR}/*/
rm -rf ${CONFIG_DIR}/.cache

find ${CONFIG_DIR} -type f -name "*.log" -print0 | xargs -0 rm -f

rm -rf ${ARTIFACTS_DIR}/notary
rm -rf ${ARTIFACTS_DIR}/partya
rm -rf ${ARTIFACTS_DIR}/partyb
rm -rf ${ARTIFACTS_DIR}/partyc
rm -rf ${ARTIFACTS_DIR}/shared/cordapps/config
rm -rf ${ARTIFACTS_DIR}/shared/node-infos

if [ -f ${ARTIFACTS_DIR}/shared/network-parameters ] ; then
  rm -f ${ARTIFACTS_DIR}/shared/network-parameters
fi
