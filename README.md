# Corda Network in Docker
![](images/Corda_Logo.png)

# Overview

- Based on [Containerizing Corda with Corda Docker Image and Docker Compose](https://www.corda.net/blog/containerizing-corda-with-corda-docker-image-and-docker-compose/).
- Corda version: 4.4 and Corda Tools Network Bootstrapper version: 4.4
- There are three parties in the network
- And one Notary
- Using [Yo! CorDapp](https://github.com/corda/samples-java/tree/master/Basic/yo-cordapp)
- Tested on MacOS 10.15.3

  ![](images/Network.png)

# Quick start

## Prerequisites

The following prerequisites are required to be installed on your system before you can run all-in-one script:

- Docker
- Docker-compose
- wget

## Execution

```shell
$ ./scripts/run.sh
```

## Clean up

```shell
$ ./scripts/clean-up.sh
```

## Configure Node Explorer

* Browse to Node Explorer UI:
  * Explorer runs at port 3000

    ```
    http://localhost:3000/
    ```

  * Note: If you are using MacOS, please use ***docker.for.mac.localhost*** instead of ***localhost*** in **Node Hostname** field.

* Set cordapps directory in settings:
  * Provide cordapps path as /cordapps in settings tab

    ```
    /cordapps
    ```

# Contribution

Your contribution is welcome and greatly appreciated. Please contribute your fixes and new features via a pull request.
Pull requests and proposed changes will then go through a code review and once approved will be merged into the project.

If you like my work, please leave me a star :)
